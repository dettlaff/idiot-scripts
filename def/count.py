##################################### Первый способ:

A = [10, 10, 23, 10, 123, 66, 78, 123]
counter = {}

for elem in A:
    counter[elem] = counter.get(elem, 0) + 1

doubles = {element: count for element, count in counter.items() if count > 1}

print(doubles)

##################################### Второй способ:

from collections import Counter
counter = Counter(A)

##################################### Третий способ:

from collections import defaultdict
counter = defaultdict(int)
for elem in A:
    counter[elem] += 1
